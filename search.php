<?php
/* blog/view.php */

$app_root = '../';
$item_title = 'Search Blog';
$page_classes = '';

$main_id = 0;

include $app_root.'include/settings.php';
include $app_root.'include/library.php';

$uncleaned_input = $_GET;
/*
$main_id = filter_input(INPUT_GET, 'blog_id', FILTER_SANITIZE_NUMBER_INT);
if(empty($main_id)) {
  die('Bad input');
}
*/
$expected_input = array('text' => '', 'category_id' => 0, 'user_id' => 0);
if(empty($uncleaned_input['category_id'])) $uncleaned_input['category_id'] = 0;
if(empty($uncleaned_input['user_id'])) $uncleaned_input['user_id'] = 0;

if(isset($_GET['trigger'])) {

    $sql_text = mysqli_real_escape_string($connect, (str_replace('_', '', $uncleaned_input['text'])));
    $sql_category_id = intval($uncleaned_input['category_id']);
    $sql_user_id = intval($uncleaned_input['user_id']);
    $query = 'SELECT * FROM blog 
    LEFT JOIN userinfo ON blog.user_id = userinfo.user_id 
    LEFT JOIN category ON blog.category_id=category.category_id ';
    if(!empty($sql_text)) {
        $where1 = ' ( blog.title LIKE "%'.$sql_text.'%" OR blog.blogtext LIKE "%'.$sql_text.'%" ) ';
    } else {
        $where1 = ' TRUE = TRUE ';
    }
    if($sql_category_id > 0) {
        $where2 = ' blog.category_id="'.$sql_category_id.'" ';
    } else {
        $where2 = " TRUE = TRUE ";
    }

    if($sql_user_id > 0) {
        $where3 = ' blog.user_id="'.$sql_user_id.'" ';
    } else {
        $where3 = " TRUE = TRUE ";
    }
    $query .= ' WHERE '.$where1. ' AND '.$where2.' AND '.$where3; 
    

//var_dump($query);
    $result = mysqli_query($connect, $query);
    if(!$result) {
        if($display_errors) {
            $errors[] = mysqli_error($connect).' - '.$query;
        } else {
            $errors[] = "Server error";
        }
    }
    $rows= mysqli_fetch_all($result, MYSQLI_ASSOC); // fetch single result
    //var_dump($row);
    $uncleaned_output_rows = $rows;

} else {
    $uncleaned_output_rows = array();
}
$html_cleaned_item_title = htmlspecialchars($item_title);

$body = '<h1>'.$html_cleaned_item_title.'</h1>';

foreach($uncleaned_output_rows as $k => $uncleaned_output) {
    $html_cleaned_title = htmlspecialchars($uncleaned_output['title']);
    $html_cleaned_blogtext = htmlspecialchars($uncleaned_output['blogtext']);
    $html_cleaned_username = htmlspecialchars($uncleaned_output['username']);
    $html_cleaned_created = htmlspecialchars($uncleaned_output['created']);
    $html_cleaned_blog_id = intval($uncleaned_output['blog_id']);
    
    if(!empty($uncleaned_output['name'])) {
    $html_cleaned_category_name = htmlspecialchars($uncleaned_output['name']);
    }
    $body .= '<div class="search_result">';
    $body .= '<h4><a href="view.php?blog_id='.$html_cleaned_blog_id.'">'.$html_cleaned_title.'</a></h4>';
    $body .= 'by '.$html_cleaned_username.' - '.$html_cleaned_created;

    $body .= '</div>';

}



$disabled = '';
$html_cleaned_category_options = get_html_options_from_db('category', 'category_id', 'name', $uncleaned_input['category_id']);
$html_cleaned_user_options = get_html_options_from_db('userinfo', 'user_id', 'username', $uncleaned_input['user_id']);
$html_cleaned_text = isset($uncleaned_input['text']) ? htmlspecialchars($uncleaned_input['text']) : '';

//$body = '<h1>'.$html_cleaned_item_title.'</h1>';
$body .= '<form action="search.php" method="get">';
$body .= '<label for="text">text</label> <input name="text" id="text" value="'.$html_cleaned_text.'" '.$disabled.'>';
//$body .= '<label for="blogtext">blog text</label><textarea name="blogtext" id="blogtext" '.$disabled.'>'.$html_cleaned_blogtext.'</textarea>';
$body .= '<br><label for="category_id">category</label> <select name="category_id" id="category_id">';
$body .= '<option value="0"> ALL </option>';
$body .= $html_cleaned_category_options;
$body .= '</select>';
$body .= '<br><label for="user_id">user</label> <select name="user_id" id="user_id">';
$body .= '<option value="0"> ALL </option>';
$body .= $html_cleaned_user_options;
$body .= '</select>';
$body .= '<input type="hidden" name="trigger" value="1">';
$body .= '<br><span class="label-spacer"></span><input type="submit" name="submit">';
$body .= '</form>';


/*
$body = '<h1>'.$html_cleaned_title.'</h1>';
$body .= '<span class="author">'.$html_cleaned_username.' - '.$html_cleaned_created.'</span>';
$body .= '<div>'.nl2br($html_cleaned_blogtext).'</div>';
if(!empty($html_cleaned_category_name)) {
$body .= '<span class="category">category: '.$html_cleaned_category_name.'</span>';
}
*/

$html_head = template_head();
$html_nav = template_menu();
$html_body = $body;
$html_foot = template_foot();

template_final_output();