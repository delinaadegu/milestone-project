<?php

/* include/library.php */

/* template_head(), template_menu(), template_foot(), template_final_output()
*/

//include 'library2.php';

function template_menu($menu_input = FALSE) {
    global $menu_array, $app_root;
    if($menu_input === FALSE) {
      $menu_input = $menu_array;
    }
    $html = '<nav>';
    foreach($menu_input as $k => $v) {
      $html .= '<a href="'.$app_root.$v.'">'.$k.'</a> | ';
    }
    $html .= '</nav>';
    return $html;
  }

  function template_head() {
    global $errors, $notices, $item_title, $app_root, $html_notice, $html_error, $page_id,
      $body_classes;
    $html_error = '';
    
  //  foreach($errors as $k=>$v) {
  //    $html_error .= '<div class="error">'.$v.'</div>';
  //  }
  //  foreach($notices as $k=>$v) {
  //    $html_notice = '<div class="notice">'.$v.'</div>';
  //  }
  
    $html = '<!doctype html>'."\n".'<html lang="en">'."\n".'<head>';
    $html .= "\n\t\t".'<meta charset="UTF-8">'."\n\t".'<title>'.$item_title.'</title>';
    $html .= "\n\t\t".'<link rel="stylesheet" href="'.$app_root.'css/styles.css">';
    $html .= "\n\t".'</head>'."\n<body"; //>";
    if(!empty($page_id)) $html .= ' id="'.$page_id.'" ';
    if(!empty($body_classes)) $html .= ' class="'.$body_classes.'" ';
    $html .= '><div id="main">';
    $html .= '<a href="'.$app_root.'blog/" id="logo_link"><img src="'.$app_root.'images/logo.png"></a>';
   // if(!empty($html_error)) $html .= "\n\t".'<div class="errorSet">'.$html_error.'</div>';
   // if(!empty($html_notice)) $html .= "\n\t".'<div class="noticeSet">'.$html_notice.'</div>';
    return $html;
  }

  function template_foot() {
    $html_foot = '</div><!--/main><footer></footer></body></html>';
    return $html_foot;
  }

  function template_final_output() {
    global $html_head, $html_nav, $html_body, $html_foot, $notices, $errors, $is_admin, $menu_admin_array;
    //print $html_head."\n<!-- nav -->\n".$html_nav;
    $html_error = '';
    foreach($errors as $k=>$v) {
      $html_error .= '<div class="error">'.$v.'</div>';
    }
    foreach($notices as $k=>$v) {
      $html_notice = '<div class="notice">'.$v.'</div>';
    }
      $ohtml_flash = '';
    if(!empty($_SESSION['flash_messages'])) {
      foreach($_SESSION['flash_messages'] as $l => $v) {
        $ohtml_flash .= '<span class="flash-m">'.$v.'</span>';
      }
      $_SESSION['flash_messages'] = [];
    }
    if(!empty($_SESSION['user_name'])) {
      $ohtml_user_name = '<span class="notice">Welcome '.htmlspecialchars($_SESSION['user_name']).'</span>';
    } else {
      $ohtml_user_name = '';
    }
    $nav2 = '';
    if($is_admin) {
      $nav2 .= template_menu($menu_admin_array);
     // die();
    }
    print $html_head."\n<!-- nav -->\n".$html_nav.$nav2;
    print $ohtml_user_name;
    if(!empty($html_error)) { 
      print "\n\t" . '<div class="errorSet">' .$html_error. '</div>';
    }
    if(!empty($html_notice)) { 
      print "\n\t".'<div class="noticeSet">'.$html_notice.'</div>';
    }
    if(!empty($ohtml_flash)) {
      print "\n\t".'<div class="flashMessages">'.$ohtml_flash.'</div>';
    }
    print "\n<!-- body -->\n".$html_body."\n<!-- foot -->\n".$html_foot;
    
  }

  function get_html_options_from_db($table, $id_col, $name_col, $current_selected=0) {
    global $connect, $display_errors, $errors;
    $query = 'SELECT '.$id_col.', '.$name_col.' FROM '.$table.';';
    //var_dump($query);
    $result = mysqli_query($connect, $query);
    if(!$result) {
      if($display_errors) {
        $errors[]=  mysqli_error($connect).' - '.$query;
      }

    }
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $options = '';
     // $options .='<option> ---- </option>';
    foreach($rows as $k=> $row) {
      $options .= "\n\t\t<option value=\"".htmlspecialchars($row[$id_col]).'"';
      if($current_selected == $row[$id_col]) {
        $options .= ' selected ';
      }
      $options .= '>'.htmlspecialchars($row[$name_col]).'</option>';
    }
    return $options;
  }