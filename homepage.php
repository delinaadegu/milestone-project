<?php
include "bloghandler.php"
?>

<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">  
<title> My Blog Posts </title>
</head>

<body>
    <div> 
        <a href="create.php"> Create a New Post </a>
    </div>
    <div class="row"> 
        <?php foreach($query as $q){ ?>
            <div class="col-4 d-flex justify-content-center align-items-center">
                <div class="card text-white bg-dark mt-5">
                    <div class="card-body" style="width: 18rem;">
                <h5 class="card-title">
                    <?php echo $q["Title"];?>
                </h5>
                <p class="card-text">
                    <?php echo $q["Content"]?>
        </p>
        <a href="view.php?Title= <?php echo $q["Title"]; ?>" class="btn btn-lite"> Read More </a> 


                </div>
                </div>
            </div>

        <?php } ?>
</div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</body>

</html>