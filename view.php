<?php
/* blog/view.php */

$app_root = '../';
$item_title = '';
$page_classes = '';

$main_id = 0;

include $app_root.'include/settings.php';
include $app_root.'include/library.php';

$uncleaned_input = $_POST;

$main_id = filter_input(INPUT_GET, 'blog_id', FILTER_SANITIZE_NUMBER_INT);
if(empty($main_id)) {
  die('Bad input');
}

$query = 'SELECT * FROM blog ' // WHERE blog_id="'.$main_id.'"'
  .' LEFT JOIN  userinfo ON blog.user_id = userinfo.user_id '
  .' LEFT JOIN category ON blog.category_id=category.category_id '
  .' WHERE blog_id="'.$main_id.'"';
$result = mysqli_query($connect, $query);
if(!$result) {
  if($display_errors) {
    $errors[] = mysqli_error($connect).' - '.$query;
  }
}
$row= mysqli_fetch_assoc($result); // fetch single result
//var_dump($row);
$uncleaned_output = $row;

$html_cleaned_title = htmlspecialchars($uncleaned_output['title']);
$html_cleaned_blogtext = htmlspecialchars($uncleaned_output['blogtext']);
$html_cleaned_username = htmlspecialchars($uncleaned_output['username']);
$html_cleaned_created = htmlspecialchars($uncleaned_output['created']);
$html_cleaned_item_title = htmlspecialchars($item_title);
if(!empty($uncleaned_output['name'])) {
  $html_cleaned_category_name = htmlspecialchars($uncleaned_output['name']);
}


/*
$disabled = '';


$body = '<h1>'.$html_cleaned_item_title.'</h1>';
$body .= '<form action="create.php" method="post">';
$body .= '<label for="name">name</label><input name="title" id="name" value="'.$html_cleaned_title.'" required '.$disabled.'>';
$body .= '<label for="blogtext">blog text</label><textarea name="blogtext" id="blogtext" '.$disabled.'>'.$html_cleaned_blogtext.'</textarea>';
$body .= '<input type="hidden" name="form_submitted" value="1">';
$body .= '<span class="label-spacer"></span><input type="submit" name="submit">';
$body .= '</form>';

*/

$body = '<h1>'.$html_cleaned_title.'</h1>';
$body .= '<span class="author">'.$html_cleaned_username.' - '.$html_cleaned_created.'</span>';
$body .= '<div>'.nl2br($html_cleaned_blogtext).'</div>';
if(!empty($html_cleaned_category_name)) {
$body .= '<span class="category">category: '.$html_cleaned_category_name.'</span>';
}


$html_head = template_head();
$html_nav = template_menu();
$html_body = $body;
$html_foot = template_foot();

template_final_output();