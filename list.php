<?php
/* blog/index.php */

$app_root = '../';
$item_title = 'List Users';
$page_classes = '';

$main_id = 0;

include $app_root.'include/settings.php';
include $app_root.'include/library.php';

//var_dump($_SESSION);
if($_SESSION['role_id'] <> 2) die('Inapprpriate permissions');

$uncleaned_input = $_POST;

$per_page = 5;
$page_num = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
$page_num = intval($page_num);
$page_start = $page_num * $per_page;
/*
$main_id = filter_input(INPUT_GET, 'blog_id', FILTER_SANITIZE_NUMBER_INT);
if(empty($main_id)) {
  die('Bad input');
}*/

$query = 'SELECT * FROM userinfo '; // WHERE blog_id="'.$main_id.'"'
 // .' LEFT JOIN  userinfo ON blog.user_id = userinfo.user_id '
 // .' ORDER BY blog_id DESC LIMIT '.($page_num * $per_page).','.$per_page;
 // .' WHERE blog_id="'.$main_id.'"';
$result = mysqli_query($connect, $query);
$num_rows = mysqli_num_rows($result);
if(!$result) {
  if($display_errors) {
    $errors[] = mysqli_error($connect).' - '.$query;
  }
}
$rows= mysqli_fetch_all($result, MYSQLI_ASSOC); // fetch single result
//var_dump($rows);
$body = '';
/*
$pages = floor($num_rows / $per_page);

$pager_html = '';
for($i = -1; $i <$pages; $i++) {
  $pager_html .= '<a href="index.php?page='.($i+1).'" class="page">'.($i+2).'</a> | ';
}*/

foreach($rows as $k => $row) {

    $uncleaned_output = $row;

    $html_id = intval($uncleaned_output['user_id']);
    //$html_user_id = intval($uncleaned_output['user_id']);
    $html_cleaned_username = htmlspecialchars($uncleaned_output['username']);
    $html_cleaned_email = htmlspecialchars($uncleaned_output['email']);
    $html_cleaned_role_id = intval($uncleaned_output['role_id']);
   // $html_cleaned_created = htmlspecialchars($uncleaned_output['created']);
   // $html_cleaned_title = htmlspecialchars($item_title);

    //$body .= '<h1><a href="view.php?blog_id='.$html_id.'">'.$html_cleaned_username.'</a></h1>';
    $body .= '<h3>'.$html_cleaned_username.'</h3>';
    $body .= 'email: <span class="author">'.$html_cleaned_email.'</span>, roleid: '.$html_cleaned_role_id;
    $body .= '<br><a href="edit.php?user_id='.$html_id.'">edit user permissions</a>';
/*
    if(!empty($_SESSION['role_id']) && $_SESSION['role_id'] == 2) {
      $body .= '<br><a class="admin" href="../admin/blogpost_admin.php?blog_id='.$html_id.'">admin</a> |'
        .' <a class="admin" href="edit.php?blog_id='.$html_id.'">edit</a>';
    } elseif (!empty($_SESSION['user_id']) && $_SESSION['user_id'] == $uncleaned_output['user_id']) {
      $body .= ' <a class="admin" href="edit.php?blog_id='.$html_id.'">edit</a>';
    }
    //$body .= '<div>'.nl2br(substr( $html_cleaned_blogtext, 0, 100)).'</div>';
*/
}
//var_dump($_SESSION);
//$body .= '<div class="pager_block">'.$pager_html.'</div>';


/*
$disabled = '';


$body = '<h1>'.$html_cleaned_item_title.'</h1>';
$body .= '<form action="create.php" method="post">';
$body .= '<label for="name">name</label><input name="title" id="name" value="'.$html_cleaned_title.'" required '.$disabled.'>';
$body .= '<label for="blogtext">blog text</label><textarea name="blogtext" id="blogtext" '.$disabled.'>'.$html_cleaned_blogtext.'</textarea>';
$body .= '<input type="hidden" name="form_submitted" value="1">';
$body .= '<span class="label-spacer"></span><input type="submit" name="submit">';
$body .= '</form>';

*/
/*
$body = '<h1>'.$html_cleaned_title.'</h1>';
$body .= '<span class="author">'.$html_cleaned_username.' - '.$html_cleaned_created.'</span>';
$body .= '<div>'.nl2br($html_cleaned_blogtext).'</div>';*/


$html_head = template_head();
$html_nav = template_menu();
$html_body = $body;
$html_foot = template_foot();

template_final_output();