<?php

/* include/settings.php */

/* fill in with your config information! */
$db_user = 'flipper';
$db_pass = '77pbj';
$db_name = 'delina_1';
$db_server = 'localhost';

$display_errors = TRUE;

if(file_exists('/home/user/james/filecheck.txt')) {
    $db_user = 'flipper';
    $db_pass = '77pbj';
    $db_name = 'delina_1';
    $db_server = 'localhost';
}

$menu_array = [
    //'home' => 'index.php',
    'blog' => 'blog/index.php'];

    session_start();
    if(empty($_SESSION['flash_messages'])) {
        $_SESSION['flash_messages'] = [];
    }
    $user_id = 0;
    $user_role = 0;

/*  NOTE: BELOW can be rewritten */

    if(!empty($_SESSION['user_id'])) {
        $user_id = intval($_SESSION['user_id']);
        $menu_array['log out'] =  'user/logout.php';
        $menu_array['create post'] = 'blog/create.php';
        $menu_array['search'] = 'blog/search.php';
    } else {
        $menu_array['register'] = 'user/registrationhandler.html';
        $menu_array['log in'] = 'user/index.html';
    }
    if(!empty($_SESSION['user_id'])) {
        $user_role = $_SESSION['role_id'];
    }

    $menu_admin_array = ['user admin' => 'user/list.php'];
    
    if($user_role >= 2){
        define('IS_ADMIN', TRUE);
        $is_admin = TRUE;
    } else {
        define('IS_ADMIN', FALSE);
        $is_admin = FALSE;
    }
    
    $connect = mysqli_connect('localhost', $db_user, $db_pass, $db_name);
    if(!$connect) {
        die('Server error');
    }
    
    
    $errors = [];
    $notices = [];
    $console = [];
    
    if(!isset($body_classes)) $body_classes = '';
    
    $error_flag = FALSE;

// from https://github.com/dariusk/wordfilter/blob/master/lib/badwords.json github user dariusk

$bad_words = [
    "abbo",
    "abo",
    "beeyotch",
    "biatch",
    "bitch",
    "chinaman",
    "chinamen",
    "chink",
    "coolie",
    "coon",
    "crazie",
    "crazy",
    "crip",
    "cuck",
    "cunt",
    "dago",
    "daygo",
    "dego",
    "dick",
    "douchebag",
    "dumb",
    "dyke",
    "eskimo",
    "fag",
    "faggot",
    "fatass",
    "fatso",
    "gash",
    "gimp",
    "golliwog",
    "gook",
    "goy",
    "goyim",
    "gyp",
    "gypsy",
    "half-breed",
    "halfbreed",
    "heeb",
    "homo",
    "hooker",
    "idiot",
    "insane",
    "insanitie",
    "insanity",
    "jap",
    "kaffer",
    "kaffir",
    "kaffir",
    "kaffre",
    "kafir",
    "kike",
    "kraut",
    "lame",
    "lardass",
    "lesbo",
    "lunatic",
    "mick",
    "negress",
    "negro",
    "nig",
    "nig-nog",
    "nigga",
    "nigger",
    "nigguh",
    "nip",
    "pajeet",
    "paki",
    "pickaninnie",
    "pickaninny",
    "prostitute",
    "pussie",
    "pussy",
    "raghead",
    "retard",
    "sambo",
    "shemale",
    "skank",
    "slut",
    "soyboy",
    "spade",
    "sperg",
    "spic",
    "spook",
    "squaw",
    "street-shitter",
    "tard",
    "tits",
    "titt",
    "trannie",
    "tranny",
    "twat",
    "wetback",
    "whore",
    "wigger",
    "wop",
    "yid",
    "zog"
];