<?php

/* admin/blogpost_admin.php */

$app_root = '../';
$item_title = '';
$page_classes = '';

include $app_root.'include/settings.php';
include $app_root.'include/library.php';

$main_id = filter_input(INPUT_GET, 'blog_id', FILTER_SANITIZE_NUMBER_INT);
if(empty($main_id)) {
  die('Bad input');
}

$uncleaned_input = $_POST;

if($_SESSION['role_id'] <> 2) {
    die('Unauthorized for this function');
}

$query = 'SELECT blog.title, blog.category_id, userinfo.username FROM blog '
    .' LEFT JOIN userinfo ON blog.user_id = userinfo.user_id '
    .' WHERE blog.blog_id = "'.$main_id.'"';

$result = mysqli_query($connect, $query);
if(!$result) {
    if($display_errors) {
        $errors[] = mysqli_error($connect).' - '.$query;
    }
}
$row = mysqli_fetch_assoc($result);
//var_dump($row);

$uncleaned_output = $row;
if(empty($uncleaned_input['category_id'])) {
    $uncleaned_input['category_id'] = 0;
}
$sql_category_id = intval($uncleaned_input['category_id']);
$sql_main_id = intval($main_id);
if(!empty($uncleaned_input['form_submitted'])) {

    $query = 'UPDATE blog SET category_id="'.$sql_category_id.'" ';
    $query .= ' WHERE blog_id="'.$sql_main_id.'"';
    $result = mysqli_query($connect, $query);
    $_SESSION['flash_messages'][] = 'Category updated on blog#'.$main_id;
    header('Location: ../blog/');
    die();
} elseif(!empty($uncleaned_input['delete'])) {
    $query = 'DELETE FROM blog WHERE blog_id="'.$sql_main_id.'"';
    $result = mysqli_query($connect, $query);
    $_SESSION['flash_messages'][] = 'Category updated on blog#'.$main_id;
    header('Location: ../blog/');
    die();
} /*else {
    $uncleaned_output = ['title' => '', 'category_id' => ''];

}*/

$html_cleaned_title = htmlspecialchars($uncleaned_output['title']);
$html_cleaned_author = htmlspecialchars($uncleaned_output['username']);
$html_cleaned_category_options = get_html_options_from_db('category', 'category_id', 'name', $uncleaned_output['category_id']);


$body = '<h1>Edit category or Delete Blog</h1>';
/* FROM https://www.foowebs.com/p14-How-to-confirm-before-submitting-a-form-with-popup-box */
$body .= '<script>
<!--
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to continue?");
if (agree)
 return true ;
else
 return false ;
}
// -->
</script>';
$body .= '<h4>'.$html_cleaned_title.' - by '.$html_cleaned_author.'</h4>';

$body .= '<form method="post" action="blogpost_admin.php?blog_id='.$main_id.'">';
$body .= '<br><br><label for="category_id">category</label>: <select name="category_id" id="category_id">';
$body .= '<option value="" selected> ---- </option>';
$body .= $html_cleaned_category_options;
$body .= '</select>';
$body .= '<input type="hidden" name="form_submitted" value="1">';
$body .= '<input type="submit">';
$body .= '</form>';

$body .= '<form method="post" action="blogpost_admin.php?blog_id='.$main_id.'">';
$body .= '<input type="submit" name="delete" value="Delete" onclick="return confirmSubmit();">';

$body .= '</form>';

$html_head = template_head();
$html_nav = template_menu();
$html_body = $body;
$html_foot = template_foot();

template_final_output();


