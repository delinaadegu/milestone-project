<?php
//xdebug_break();
/* blog/edit.php */



$app_root = '../';
$item_title = 'Edit new blog';
$page_classes = ' edit blog ';

$main_id = 0;

include $app_root.'include/settings.php';
include $app_root.'include/library.php';

if(empty($_SESSION['user_id'])) {
    die('User must be logged in');
}

$uncleaned_input = $_POST;

$input_keys = ['title', 'blogtext'];

$now_date = date('Y-m-d H:i:s');
$main_id = filter_input(INPUT_GET, 'blog_id', FILTER_SANITIZE_NUMBER_INT);
$sql_main_id = intval($main_id);

$query = 'SELECT * FROM blog WHERE blog_id="'.$sql_main_id.'"';
$result = mysqli_query($connect, $query);
$row = mysqli_fetch_assoc($result);

$original_item_row = $row;


if(!empty($uncleaned_input['form_submitted'])) {
    $query_title = mysqli_real_escape_string($connect, $uncleaned_input['title']);
    $query_blogtext = mysqli_real_escape_string($connect, $uncleaned_input['blogtext']);
    $query_user_id = $user_id;
    $query_now_date = mysqli_real_escape_string($connect, $now_date);
    $query_category_id = intval($uncleaned_input['category_id']);
    if(empty($uncleaned_input['title'])) {
        $error_flag = TRUE;
        $errors[]= 'Title field is mandatory';
    }
    if(empty($uncleaned_input['blogtext'])) {
        $error_flag = TRUE;
        $errors[]= 'Blogtext field is mandatory';
    }

    /* need to do bad word checking routine */

    foreach ($bad_words as $k => $v) {
        $check = strpos($uncleaned_input['blogtext'], $v);
        if($check !== FALSE) {
            $error_flag = TRUE;
            $errors[] = 'Text contains inappriate language';
        }
        
    }

    if(!$error_flag){
        
      //  $query = 'INSERT INTO blog (title, blogtext, user_id, category_id, editd, updated_by) '
      //      .' VALUES ("'.$query_title.'", "'.$query_blogtext.'", "'.$query_user_id.'", "'.$query_category_id.'", "'.$query_now_date.'", "'.$query_user_id.'");';
        $query = 'UPDATE blog SET title="'.$query_title.'", blogtext="'.$query_blogtext.'", category_id="'.$query_category_id.'", updated_date="'.$query_now_date.'", updated_by="'.$query_user_id.'"'
            .' WHERE blog_id="'.$sql_main_id.'";';
        $result = mysqli_query($connect, $query);
        if(!$result) {
            $error_flag = TRUE;
            if($display_errors) {
                $errors[]= mysqli_error($connect).' - '.$query;
                
            } else {
               // $error_flag = TRUE;
                $errors[]= 'Error - is your title unique? [Must be different from existing titles]';
            }
        }
        //$new_blog_id = mysqli_insert_id($connect);
        //$main_id = $new_blog_id;
        if(!$error_flag) {
            $notices[] = 'New blog editd: <a href="view.php?blog_id='.$main_id.'">#'.$main_id.'</a>';
            $_SESSION['flash_messages'][] = 'New blog editd: <a href="view.php?blog_id='.$main_id.'">#'.$main_id.'</a>';
            header('Location: ../blog/');
            exit();
            //$uncleaned_output = $uncleaned_input;
        } else {
            $uncleaned_output = $uncleaned_input; //['title' => '', 'blogtext' => '', 'category_id' => ''];
        }
    } else {
        if($error_flag) {
            $uncleaned_output = $uncleaned_input;
        } else {
           // $uncleaned_output = ['title' => '', 'blogtext' => '', 'category_id' => ''];
           $uncleaned_output = $uncleaned_input;
        }
    }
} else {
    //$uncleaned_output = ['title' => '', 'blogtext' => '', 'category_id' => ''];
    //$uncleaned_output = $uncleaned_input;
    $uncleaned_output = $original_item_row;
}

$html_cleaned_title = htmlspecialchars($uncleaned_output['title']);
$html_cleaned_blogtext = htmlspecialchars($uncleaned_output['blogtext']);
$html_cleaned_item_title = htmlspecialchars($item_title);
$html_cleaned_main_id = intval($main_id);

$disabled = '';

$html_cleaned_category_options = get_html_options_from_db('category', 'category_id', 'name', $uncleaned_output['category_id']);

$body = '<h1>'.$html_cleaned_item_title.'</h1>';
$body .= '<form action="edit.php?blog_id='.$html_cleaned_main_id.'" method="post">';
$body .= '<label for="name">name</label>: <input name="title" id="name" value="'.$html_cleaned_title.'" required '.$disabled.'>';
$body .= '<br><br><label for="blogtext">blog text</label>: <textarea name="blogtext" id="blogtext" required '.$disabled.'>'.$html_cleaned_blogtext.'</textarea>';
$body .= '<br><br><label for="category_id">category</label>: <select name="category_id" id="category_id">';
$body .= '<option value="" selected> ---- </option>';
$body .= $html_cleaned_category_options;
$body .= '</select>';
$body .= '<input type="hidden" name="form_submitted" value="1">';
$body .= '<br><br><span class="label-spacer"></span><input type="submit" name="submit">';
$body .= '</form>';

$html_head = template_head();
$html_nav = template_menu();
$html_body = $body;
$html_foot = template_foot();

template_final_output();